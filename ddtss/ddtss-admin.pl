use strict;

#use Encode;
use DDTSS_Pg;

if(scalar(@ARGV) == 0)
{
   die "Need option\n";
}
if($ARGV[0] eq "find")
{
  my $email = $ARGV[1] || '';
  
  my $db = DDTSS_Open_Read();

  print "Looking for $email\n";
    
  ddtss_match($db, "aliases/", sub {
      my($key,$value) = @_;
      return if $key !~ m,^aliases/([^/]+)$,;
      my $user = $1;
#      print "$key,$value\n";
      return if $value !~ $email;
      
      print "$user: $value\n";
  });
  
  DDTSS_Close($db);
}
if($ARGV[0] eq "password")
{
  my $username = $ARGV[1] or die "Require username\n";
  my $newpass = $ARGV[2];
  my $email;
  my $pass;
  
  my $db = DDTSS_Open_Write();
  
  if($db->get("aliases/$username", $email))
  {
    die "Couldn't find user '$username'\n";
  }
  if($db->get("aliases/$username/password", $pass))
  {
    die "Couldn't get password\n";
  }
  print "Password: '$pass'\n";
  if( defined $newpass )
  {
    $db->put("aliases/$username/password", $newpass);
    print "Password changed\n";
  }
  
  DDTSS_Close($db);
}

  