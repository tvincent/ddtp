use strict;

use DB_File::Lock;
use Encode;
my %hash;
my $db_hash;

$db_hash = "ddtss2.db";
#$db_hash = "ddtss2.db-1158265963";

tie %hash,  'DB_File::Lock', $db_hash, O_RDONLY, 0666, $DB_BTREE, "read"
  or die "Cannot tie $db_hash ($!)\n";

my @todo;
foreach my $key (keys %hash)
{
#  if( $key =~ m,^../requests/, )
#  { push @todo, $key }
#  next unless $key =~ m,^([^/]+)/logs/(\d+)/(.+),;
#  next if $2 < 1220047200;
  
  next unless $key =~ /^it/;

  print "Key: $key: $hash{$key}\n";
#  print "Key: $key\n";
  
#  $key = "$1/done/$3";
  
#  print "Key: $key: $hash{$key}\n";
  
  eval {
    my $str = Encode::decode("utf8", $hash{$key}, Encode::FB_CROAK);
  };
  if($@)
  {
    print "UTF-8 coding error: $key\n";
  }
}

untie %hash;

#print @todo;


exit;

tie %hash,  'DB_File::Lock', $db_hash, O_RDWR, 0666, $DB_BTREE, "write"
  or die "Cannot tie $db_hash ($!)\n";

#for my $key (@todo)
#{
#  delete $hash{$key};
#}
exit;

foreach my $key (keys %hash)
{
  if( $key =~ m,pt_PT/, )
  {
    my $new_key = $key;
    $new_key =~ s/pt_PT/pt/;
    $hash{$new_key} = $hash{$key};
    delete $hash{$key};
    print STDERR "$key -> $new_key\n";
  }
}
my @langs = qw(da de es fi fr hu it ja km_KH nl pl sv zh_CN pt pt_BR eo zh_TW ca cs vi ml ru sk);
$hash{'langs'} = join(",", @langs);

untie %hash;

print join(" ", @todo), "\n";
