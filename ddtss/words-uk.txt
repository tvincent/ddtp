2d	двовимірний
3d	тривимірний
application	застосунок
backup	резервування, резервна копія
binary	двійковий
binding	оправа, зв’зування, прив’язка
bug	вада
bundle	клунок
catalog	тека, каталог
collection	набір, зібрання, збірка
configuration	налаштування, склад обладнання
common files	спільні файли
compressed	стиснений
cross-platform	мультиплатформовий
customizable	налаштовуваний
custom	нетиповий
daemon	фонова служба
debug	налагоджувати
debugging symbols	символи налагодження
default	типовий, стандартний
desktop	стільниця
directory	тека, каталог
dispatch	передавати керування, координувати, відправити, розпорядження
display	екран, відображати, відтворювати, показувати
dummy	фіктивний
dump	аварійне знімання, скинути
enhanced	покращений, вдосконалений
extension	розширення
framework	(інфра)структура, каркас
frontend	зовнішній інтерфейс
game	гра, іграшка, забавка,  цяцька, розвага, виграшка
GUI	ГІК
implementation	реалізація
lightweight	легкий
log	журнал
meta-package	збірний пакунок
metapackage	збірний пакунок
multicast	групова/широкомовна трансляція/передача
package	пакунок
packet	пакет
parse	аналіз, дослідження
parser	аналізатор
patch	латка
plugin	додаток
pool	схованка
powerful	потужний
provide	надавати
privilege	привілей
pull	тягти
purge	очистити
puzzle	головоломка, загадка, морока. ребус, шарада
raw	необроблений, неопрацьований
render	відтворення, візуалізація, промальовка
remote	віддалений, дистанційний
repository	сховище, архів
resolution	роздільна здатність
resolve	розв’язати
revision control	(система) керування версіями
runtime	виконавчий
queue	черга
script	сценарій
service	служба
shared	колективний
shell	оболонка
shooter	стрілялка
source	вихідний, джерельний
tag	мітка
transitional	перехідний
utility	утиліта
wraper	обгортка
widget	візуальний компонент, віджет
