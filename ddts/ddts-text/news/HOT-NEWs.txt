Sat Apr 16 17:17:44 CEST 2011
  Now I know how to give news to all teams. I also know
  a little bit about how to configure language-dependent settings
  such as number of reviews and authorization for
  anonymous work.
  
  If you want to change something to your language settings, please
  ask.
  
  Christian Perrier <bubulle@debian.org>
